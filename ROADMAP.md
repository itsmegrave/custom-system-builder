# Planned Features

# Labels

-   🚧️ : In progress
-   ✔️: Done waiting for release
-   💤️ : Waiting for development

# Scheduled for 2.4.0

-   ❌️ custom-system-builder#5 - Ability to add items to other items => Postponed to post v11 compatibility update
-   🚧️ custom-system-builder#155 - Conditional item & active effects modifiers

# 2.2.0

-   ✔️custom-system-builder#82 Added Drag&Drop on components to allow movement everywhere

const { I } = inject();
const { getActorId } = require('../utils.js');

/**********************************************/
/*            TEMPLATE STEPS                  */
/**********************************************/

When(/^I select '(.*)' as panel layout$/, (layout) => {
    I.selectOption('#panelFlow', layout);
});

When(/^I select '(.*)' as panel alignment$/, (alignment) => {
    I.selectOption('#panelAlign', alignment);
});

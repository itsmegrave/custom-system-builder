const { getActorId } = require('../utils.js');
const { I } = inject();

/**********************************************/
/*            TEMPLATE STEPS                  */
/**********************************************/

When(/^I type '(.*)' as radio button label$/, (contents) => {
    I.fillField('#radioButtonLabel', contents);
});

When(/^I type '(.*)' as radio button group$/, (contents) => {
    I.fillField('#radioButtonGroup', contents);
});

When(/^I type '(.*)' as radio button value$/, (contents) => {
    I.fillField('#radioButtonValue', contents);
});

/**********************************************/
/*            CHARACTER STEPS                 */
/**********************************************/

When(/^I click on the '(.*)' checkbox in the '(.*)' character$/, (fieldKey, characterName) => {
    let characterId = getActorId(characterName);

    I.click(`#CharacterSheet-Actor-${characterId} .${fieldKey} input[type="checkbox"]`);
});

Then(/^The '(.*)' checkbox is '(checked|not checked)' in the '(.*)' character$/, (fieldKey, action, characterName) => {
    let characterId = getActorId(characterName);

    switch (action) {
        case 'checked':
            I.seeCheckboxIsChecked(`#CharacterSheet-Actor-${characterId} .${fieldKey} input[type="checkbox"]`);
            break;
        case 'not checked':
            I.dontSeeCheckboxIsChecked(`#CharacterSheet-Actor-${characterId} .${fieldKey} input[type="checkbox"]`);
            break;
    }
});

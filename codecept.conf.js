const chromiumArgs = [
    '--disable-web-security',
    '--ignore-certificate-errors',
    '--disable-infobars',
    '--allow-insecure-localhost',
    '--disable-device-discovery-notifications',
    '--window-size=1600,900'
];

exports.config = {
    output: './tests/output/',
    helpers: {
        Puppeteer: {
            url: 'http://localhost:30000',
            show: true,
            windowSize: '1600x900',
            waitForNavigation: 'networkidle0',
            chrome: {
                args: chromiumArgs
            }
        },
        ResembleHelper: {
            require: 'codeceptjs-resemblehelper',
            screenshotFolder: './tests/output/',
            baseFolder: './tests/expected_data/screenshots/base/',
            diffFolder: './tests/expected_data/screenshots/diff/'
        }
    },
    include: {
        I: './steps_file.js'
    },
    mocha: {
        reporterOptions: {
            reportDir: 'output'
        }
    },
    bootstrap: null,
    timeout: null,
    teardown: null,
    hooks: [],
    gherkin: {
        features: ['./tests/features/*.feature', './tests/features/**/*.feature'],
        steps: ['./tests/step_definitions/steps.js']
    },
    plugins: {
        screenshotOnFail: {
            enabled: true
        },
        tryTo: {
            enabled: true
        },
        retryFailedStep: {
            enabled: false
        },
        retryTo: {
            enabled: true
        },
        eachElement: {
            enabled: true
        },
        pauseOnFail: {}
    },
    stepTimeout: 0,
    stepTimeoutOverride: [
        {
            pattern: 'wait.*',
            timeout: 0
        },
        {
            pattern: 'amOnPage',
            timeout: 0
        }
    ],
    tests: './tests/*.test.js',
    name: 'custom-system-builder'
};
